import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterresturant/screens/resturant.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.orangeAccent, // navigation bar color
      statusBarColor: Colors.orangeAccent, // status bar color
    ));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pricilla eFoods',
      theme: ThemeData(
        primaryColor: Colors.grey[800],
        cursorColor: Colors.orange,
      ),
      home: ResturantXD(),
    );
  }
}
